package nico.styTool;

import android.app.Application;
import android.content.Context;

import com.blankj.utilcode.util.AppUtils;
import com.drake.engine.base.EngineKt;
import com.example.blade.util.accessibility.ToolsService;
import com.kongzue.baseframework.BaseFrameworkSettings;
import com.kongzue.dialogx.DialogX;
import com.lzf.easyfloat.EasyFloat;
import com.simple.spiderman.SpiderMan;

import cn.vove7.andro_accessibility_api.AccessibilityApi;
import me.weishu.reflection.Reflection;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SpiderMan.init(this);
        BaseFrameworkSettings.DEBUGMODE = AppUtils.isAppDebug();
        BaseFrameworkSettings.BETA_PLAN = AppUtils.isAppDebug();
        EasyFloat.init(this, AppUtils.isAppDebug());
        AccessibilityApi.BASE_SERVICE_CLS = ToolsService.class;
        /*DialogSettings.modalDialog = true;
        DialogSettings.isUseBlur = false;
        DialogSettings.contentTextInfo = new TextInfo().setBold(true);
        DialogSettings.style = DialogSettings.STYLE.STYLE_MIUI;*/
       /* DialogSettings.dialogLifeCycleListener = (new DialogLifeCycleListener() {

            @Override
            public void onCreate(BaseDialog dialog) {

            }

            @Override
            public void onShow(BaseDialog dialog) {

            }

            @Override
            public void onDismiss(BaseDialog dialog) {

            }
        });*/
        new Initialization();
        new Net(this);
        EngineKt.initEngine(this);
        DialogX.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        Reflection.unseal(base);
    }
}

package nico.styTool.inputBox

/*
写的有点烂，样板代码太多 以后再优化

class InputBoxDialog(
    activity: FragmentActivity,
    function: ButtonsBuilder = ButtonsBuilder(),
    callback: (String, String) -> Unit
) {
    init {
        BottomDialogActivity.builder(activity) {
            content(object : ContentBuilder() {
                override val layoutRes = R.layout.search_input

                override fun init(view: View) {
                    view.box_input.apply {
                        doAfterTextChanged {
                            inputText = it.toString()
                            if (it != null) {
                                if (it.length > 250) {
                                    ToastUtils.showShort("字符超过250禁止自动保存")
                                } else {
                                    spUtils.put("${activity}_0", it.toString())
                                }
                            }
                        }
                        setText(spUtils.getString("${activity}_0"))
                    }
                    view.box_input2.apply {
                        doAfterTextChanged {
                            inputText2 = it.toString()
                            if (it != null) {
                                if (it.length > 250) {
                                    ToastUtils.showShort("字符超过250禁止自动保存")
                                } else {
                                    spUtils.put("${activity}_1", it.toString())
                                }
                            }
                        }
                        setText(spUtils.getString("${activity}_1"))
                    }

                }

                override fun updateContent(type: Int, data: Any?) = Unit
            }).apply {
                //expandable = false
                peekHeightProportion = 0.4f
            }
            buttons {

                positiveButton {
                    if (visibility) {
                        if (inputText == text || inputText2 == text) {
                            ToastUtils.showShort("你*的,没输入呢")
                        } else {
                            it.dismiss()
                            callback(inputText, inputText2)
                        }
                    } else {
                        if (inputText != text) {
                            it.dismiss()
                            callback(inputText, null.toString())
                        }
                    }
                }
                function.negativeButton()
            }
        }
    }

    companion object {
        private const val text = ""
        private var inputText = text
        private var inputText2 = text
        private val spUtils = SPUtils.getInstance()
    }
} */
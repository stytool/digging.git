package nico.styTool

import android.app.Application
import com.blankj.utilcode.util.ClipboardUtils
import com.blankj.utilcode.util.ToastUtils
import com.kongzue.dialogx.dialogs.BottomMenu

object Utils {
    fun getApp(): Application = com.blankj.utilcode.util.Utils.getApp()

    fun setBottomMenu(message: String = "", menuList: Array<String>, callback: (Int) -> Unit = {}) {
        BottomMenu.show(menuList)
            .setMessage(if (message == "") null else message)
            .setOnMenuItemClickListener { _, _, index ->
                callback.invoke(index)
                false
            }
    }

    fun setToast(message: String) = ToastUtils.showLong(message)

    fun setClipboard(text: String?) {
        if (text == "") {
            setToast("Clipboard/错误")
        } else {
            ClipboardUtils.copyText(text).apply {
                setToast("复制成功")
            }
        }
    }
}
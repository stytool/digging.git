package nico.styTool

import android.os.Build
import com.blankj.utilcode.util.ThreadUtils
import com.bumptech.glide.Glide
import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.interfaces.Layout
import com.kongzue.baseframework.util.JumpParameter
import com.zackratos.ultimatebarx.library.UltimateBarX
import kotlinx.android.synthetic.main.activity_splash.*

@Layout(R.layout.activity_splash)
class SplashActivity : BaseActivity() {


    override fun initViews() {
        supportActionBar?.also {
            it.hide()
            UltimateBarX.create(UltimateBarX.STATUS_BAR)
                .transparent()
                .light(true)
                .apply(this)
        }
        ThreadUtils.runOnUiThreadDelayed({
            jump(MainActivity::class.java, rxScaleImageView)
            handleFinish()
        }, 2500L)
        Glide.with(this).load(load).into(rxScaleImageView)
    }

    private fun handleFinish() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition()
        } else {
            finishActivity()
        }
    }

    override fun initDatas(parameter: JumpParameter?) {

    }

    override fun setEvents() {

    }

    companion object {
        private const val load =
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1599230707636&di=05dca4a657fa66a4aec1031c4ac955a3&imgtype=0&src=http%3A%2F%2Finews.gtimg.com%2Fnewsapp_match%2F0%2F8132731115%2F0.jpg"

    }

}
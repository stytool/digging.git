package nico.styTool.click

import android.app.WallpaperManager
import android.graphics.Bitmap
import androidx.fragment.app.FragmentActivity
import cn.vove7.bottomdialog.BottomDialog
import cn.vove7.bottomdialog.builder.MessageContentBuilder
import cn.vove7.bottomdialog.builder.buttons
import cn.vove7.bottomdialog.builder.message
import cn.vove7.bottomdialog.builder.title
import com.blankj.utilcode.util.ColorUtils
import com.blankj.utilcode.util.FileUtils
import com.blankj.utilcode.util.ImageUtils
import com.blankj.utilcode.util.PathUtils.getExternalAppPicturesPath
import nico.styTool.Utils.getApp


class PictureTool(requireActivity: FragmentActivity) {

    init {
        val dialog = BottomDialog.builder(requireActivity) {
            title("Hello")
            message("")
            buttons {
                positiveButton { it.dismiss() }
            }
        }
        if (wallpaperManager2.peekDrawable() == null) {
            dialog.updateContent<MessageContentBuilder> {
                text = "啊这???\n读不出来数据\n$Suggest"
            }
        } else {
            val src = ImageUtils.drawable2Bitmap(wallpaperManager2.peekDrawable())
            val filePath = file + "/" + ColorUtils.getRandomColor() + ".png"
            val format = Bitmap.CompressFormat.PNG
            if (ImageUtils.save(src, filePath, format)) {
                FileUtils.notifySystemToScan(filePath)
                dialog.updateContent<MessageContentBuilder> {
                    text = "壁纸已保存至:\n$filePath"
                }
            } else {
                dialog.updateContent<MessageContentBuilder> {
                    text = "壁纸保存失败,$Suggest"
                }
            }
        }
    }

    companion object {
        private val file = getExternalAppPicturesPath()
        private val wallpaperManager2 = WallpaperManager.getInstance(getApp())
        private const val Suggest = "建议联系开发者\n2284467793@qq.com 谢谢!"
    }
}


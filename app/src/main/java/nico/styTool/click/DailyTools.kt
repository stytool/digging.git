package nico.styTool.click


import android.annotation.SuppressLint
import android.view.Gravity
import androidx.fragment.app.FragmentActivity
import com.example.blade.ReaderUtil
import com.example.blade.util.battery.BatteryUtils
import com.lzf.easyfloat.EasyFloat
import com.lzf.easyfloat.anim.AppFloatDefaultAnimator
import com.lzf.easyfloat.anim.DefaultAnimator
import com.lzf.easyfloat.enums.ShowPattern
import com.lzf.easyfloat.interfaces.OnDisplayHeight
import com.lzf.easyfloat.interfaces.OnInvokeView
import com.lzf.easyfloat.utils.DisplayUtils
import kotlinx.android.synthetic.main.float_app_power.view.*
import kotlinx.android.synthetic.main.float_app_time.view.*
import nico.styTool.R
import nico.styTool.Utils.getApp

class DailyTools {
    fun time(activity: FragmentActivity) {
        val floatTag = "Float_time"
        EasyFloat.with(activity)
            .setLayout(R.layout.float_app_time, OnInvokeView { view ->

                view.tv_time.setOnLongClickListener {
                    EasyFloat.dismissAppFloat(floatTag)
                    EasyFloat.dismiss(activity, floatTag)
                    false
                }

            })

            .setShowPattern(ShowPattern.ALL_TIME)
            .setTag(floatTag)
            .setDragEnable(true)
            .setMatchParent(widthMatch = false, heightMatch = false)
            .setAnimator(DefaultAnimator())
            .setGravity(Gravity.CENTER)
            .setAppFloatAnimator(AppFloatDefaultAnimator())
            .setDisplayHeight(OnDisplayHeight { context -> DisplayUtils.rejectedNavHeight(context) })
            .registerCallback {
                show {

                }
            }
            .show()
    }

    @SuppressLint("SetTextI18n")
    fun power(activity: FragmentActivity) {
        val floatTag = "Float_power"
        EasyFloat.with(activity)
            .setLayout(R.layout.float_app_power, OnInvokeView { view ->
                BatteryUtils.registerBatteryStatusChangedListener { status ->
                    view.tv_power.text = status.toString()
                }
                view.tv_power.setOnLongClickListener {
                    EasyFloat.dismissAppFloat(floatTag)
                    EasyFloat.dismiss(activity, floatTag)
                    false
                }

            })
            .setShowPattern(ShowPattern.ALL_TIME)
            .setTag(floatTag)
            .setDragEnable(true)
            .setMatchParent(widthMatch = false, heightMatch = false)
            .setAnimator(DefaultAnimator())
            .setGravity(Gravity.CENTER)
            .setAppFloatAnimator(AppFloatDefaultAnimator())
            .setDisplayHeight(OnDisplayHeight { context -> DisplayUtils.rejectedNavHeight(context) })
            .registerCallback {
                show {

                }
            }
            .show()
    }
}
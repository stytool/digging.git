package nico.styTool.click

import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.util.RegexUtils.isMatch
import com.blankj.utilcode.util.ToastUtils
import com.example.blade.InputBox
import com.example.blade.InputBox.Companion.SaveData
import com.example.blade.util.shell.MyShell

class SystemTool(requireActivity: FragmentActivity) {

    init {
        InputBox.Builder(requireActivity, Hint = arrayOf("0-999"))
            .setPositiveButton {
                val str = SaveData
                if (isMatch("[0-9]*", str)) {
                    MyShell().shell("dumpsys battery set level $str")
                } else {
                    ToastUtils.showShort("数字呢")
                }
            }
            .setNegativeButton("恢复默认") {
                MyShell().shell("dumpsys battery reset")
            }
            .create()
    }
}
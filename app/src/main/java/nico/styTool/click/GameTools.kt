package nico.styTool.click

import android.view.Gravity
import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.util.ToastUtils
import com.example.blade.ReaderUtil
import com.lzf.easyfloat.EasyFloat
import com.lzf.easyfloat.anim.AppFloatDefaultAnimator
import com.lzf.easyfloat.anim.DefaultAnimator
import com.lzf.easyfloat.enums.ShowPattern
import com.lzf.easyfloat.interfaces.OnDisplayHeight
import com.lzf.easyfloat.utils.DisplayUtils
import kotlinx.android.synthetic.main.float_app_iii.view.*
import nico.styTool.R

class GameTools {
    fun wzMark() {
        nico.styTool.Utils.setToast("感觉这功能没软用,已懒的写 可以用a萌")
        /*val dialog = MaterialDialog(activity, BottomSheet(LayoutMode.WRAP_CONTENT)).show {
            customView(R.layout.activity_main_wz_mark)
            lifecycleOwner(activity)
        }
        dialog.onShow {
            val i = it.getCustomView()
            i.switchWidget_wz.setOnCheckedChangeListener { i: Boolean ->

            }
        }*/
    }

    fun mark(activity: FragmentActivity) {
        val floatTag = "Float_III"
        EasyFloat.with(activity)
            .setLayout(R.layout.float_app_iii) { view ->

                view.VAL_III.setOnLongClickListener {
                    EasyFloat.dismissAppFloat(floatTag)
                    EasyFloat.dismiss(activity, floatTag)
                    false
                }

            }
            .setShowPattern(ShowPattern.ALL_TIME)
            .setTag(floatTag)
            .setDragEnable(false)
            .setMatchParent(widthMatch = false, heightMatch = false)
            .setAnimator(DefaultAnimator())
            .setGravity(Gravity.CENTER)
            .setAppFloatAnimator(AppFloatDefaultAnimator())
            .setDisplayHeight(OnDisplayHeight { context -> DisplayUtils.rejectedNavHeight(context) })
            .registerCallback {
                show {

                }
            }
            .show()
    }

    fun kinna() {


    }
}
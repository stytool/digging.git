package nico.styTool

import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.databinding.ViewDataBinding
import com.blankj.utilcode.constant.PermissionConstants
import com.blankj.utilcode.util.PermissionUtils
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.ToastUtils
import com.bumptech.glide.Glide
import com.drake.engine.base.EngineActivity
import com.example.blade.PrivacyPolicy
import com.example.blade.RxShow
import kotlinx.android.synthetic.main.activity_hello.*
import nico.styTool.bottomDialog.RateBottom
import nico.styTool.fragment.BottomRight
import nico.styTool.fragment.Fragment2
import qiu.niorgai.StatusBarCompat


class HelloActivity : EngineActivity<ViewDataBinding>(R.layout.activity_hello) {

    override fun initData() {
        addShortcut()
    }

    override fun initView() {
        setEvents()
        setSupportActionBar(toolbar.apply {
            StatusBarCompat.setStatusBarColorForCollapsingToolbar(
                this@HelloActivity,
                appBarLayout.apply {
                },
                collapsingToolbarLayout.apply {
                    title = " "
                },
                this,
                0
            )
        })
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.hello_world, Fragment2())
            .commit()
        Glide.with(this).load(load).into(imageView)
        RightClick.setOnClickListener {
            BottomRight(this, it)
        }
    }

    private fun addShortcut() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            val shortcutManager = getSystemService(ShortcutManager::class.java)
            val shortcutInfo = ShortcutInfo.Builder(this, "id1")
                .setShortLabel("Web site")
                .setLongLabel("Open this web site")
                .setIcon(Icon.createWithResource(this, R.mipmap.ic_launcher))
                .setIntents(
                    arrayOf(
                        Intent(
                            Intent.ACTION_MAIN,
                            Uri.EMPTY,
                            this,
                            MainActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK),
                        Intent(Intent.ACTION_VIEW, Uri.parse("https://www.example.com/"))
                    )
                )

                .build()
            shortcutManager.addDynamicShortcuts(mutableListOf(shortcutInfo))
        }

    }

    private fun setEvents() {
        PermissionUtils
            .permission(PermissionConstants.STORAGE, PermissionConstants.PHONE)
            .callback(object : PermissionUtils.SimpleCallback {
                override fun onGranted() {
                    PrivacyPolicy(this@HelloActivity)
                    if (SPUtils.getInstance().getBoolean("SP_IS_FIRST_ENTER_APP")) {
                        RxShow(this@HelloActivity)
                    }
                    RateBottom(this@HelloActivity)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        if (Environment.isExternalStorageLegacy()) {
                            //nico.styTool.Utils.setToast("应用以兼容模式运行")
                        } else {
                            Utils.setToast("应用以分区存储特性运行")
                        }
                    }
                }

                override fun onDenied() {
                    Utils.setToast("申请权限失败")
                }
            })
    }

    companion object {
        private const val load =
            "http://images.h128.com/upload/202010/22/27304a5499aa6e8527986ff36d924cf6.jpg"
    }

}
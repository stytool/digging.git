package nico.styTool

import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.util.AppManager


class Initialization {
    init {
        AppManager.setOnActivityStatusChangeListener(object :
            AppManager.OnActivityStatusChangeListener {
            override fun onActivityCreate(activity: BaseActivity?) {
                if (activity != null) {
                    synchronized(activity) {
                        /*BaseBox = activity*/
                    }
                }
            }

            override fun onActivityDestroy(activity: BaseActivity) = Unit

            override fun onAllActivityClose() = Unit
        })
    }
}
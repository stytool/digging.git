package nico.styTool

import androidx.fragment.app.Fragment
import androidx.viewpager2.widget.ViewPager2
import com.blankj.utilcode.util.ClickUtils
import com.blankj.utilcode.util.ThreadUtils
import com.blankj.utilcode.util.UiMessageUtils
import com.fxn.OnBubbleClickListener
import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.interfaces.Layout
import com.kongzue.baseframework.util.JumpParameter
import com.zackratos.ultimatebarx.library.UltimateBarX
import kotlinx.android.synthetic.main.activity_main.*
import nico.styTool.fragment.Fragment1
import nico.styTool.fragment.Fragment2
import nico.styTool.viewPager.adapter.FragmentLazyStateAdapter

@Layout(R.layout.activity_main)
class MainActivity : BaseActivity() {
    override fun initViews() {
        supportActionBar?.also {
            it.hide()
        }
        permission()
    }

    override fun initDatas(parameter: JumpParameter?) {
        UltimateBarX.create(UltimateBarX.STATUS_BAR)
            .transparent()
            .light(true)
            .apply(this)
    }


    override fun setEvents() {
        var itClick = 0
        bubbleTabBar.addBubbleListener(object : OnBubbleClickListener {
            override fun onBubbleClick(id: Int) {
                when (id) {
                    R.id.home -> {
                        viewpager.currentItem = 0
                        itClick++
                        when (itClick) {
                            1 -> ThreadUtils.runOnUiThreadDelayed({
                                itClick = 0
                            }, 500L)
                            2 -> {
                                UiMessageUtils.getInstance()
                                    .send(R.id.utilCodeUiMessageAddListenerId)
                                itClick = 0
                            }
                        }

                    }
                    R.id.log -> viewpager.currentItem = 1
                    //R.id.doc -> viewpager.currentItem = 2
                    R.id.setting -> {

                    }
                }
            }
        })
        val item = mutableListOf<Fragment>()
        item.add(Fragment1())
        item.add(Fragment2())
        //item.add(Fragment3())
        //item.add(Fragment4())
        viewpager.adapter = FragmentLazyStateAdapter(this, item)
        viewpager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                bubbleTabBar.setSelected(position)
            }
        })
    }

    private fun permission() {

    }

    fun current() {
        viewpager.currentItem = 1
    }

    override fun onBackPressed() {
        ClickUtils.back2HomeFriendly("再按一次退出")
    }
}
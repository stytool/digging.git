package nico.styTool

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS
import android.service.quicksettings.TileService
import android.view.WindowManager
import androidx.annotation.RequiresApi
import cn.vove7.bottomdialog.builder.ClickListenerSetter
import com.blankj.utilcode.util.NotificationUtils.setNotificationBarVisibility
import com.blankj.utilcode.util.PermissionUtils
import com.blankj.utilcode.util.PermissionUtils.isGrantedDrawOverlays
import com.blankj.utilcode.util.PermissionUtils.requestDrawOverlays
import com.blankj.utilcode.util.ServiceUtils
import com.blankj.utilcode.util.ToastUtils
import com.example.blade.util.accessibility.ToolsService
import nico.styTool.service.BasicService

@RequiresApi(api = Build.VERSION_CODES.N)
class QuickStartTileService : TileService() {
    override fun onClick() {
        super.onClick()
        setNotificationBarVisibility(false)/*反射调用收起通知栏*/
        requestDrawOverlays(object : PermissionUtils.SimpleCallback {
            override fun onGranted() {
                show()
            }

            override fun onDenied() {
                ToastUtils.showShort("被拒绝权限,无法显示")
            }
        })
    }

    private fun action(p2: (() -> Unit)) {
        if (!ServiceUtils.isServiceRunning(ToolsService::class.java)) {
            Intent(ACTION_ACCESSIBILITY_SETTINGS).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(this)
            }
        } else {
            p2.invoke()
        }
    }

    private fun show() {
        AlertDialog.Builder(this)
            .setTitle("辅助功能(By：颜咚)") //.setMessage("By：颜咚")
            .setItems(starr) { _, which ->
                when (which) {
                    0, 1, 2, 3, 4, 5, 6, 7 -> {
                        action {
                            BasicService(which, this)
                        }
                    }
                    8, 9, 10, 11 -> {
                        BasicService(which, this)
                    }
                }
            }
            /*.setPositiveButton("") { dialog, which ->

            }*/
            .create().apply {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    window?.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
                } else {
                    window?.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
                }

                show()
            }

    }


    companion object {
        private val starr =
            arrayOf(
                "QQ名片自动回赞",
                "QQ消息扣字器",
                "QQ删除历史名片赞",
                "分析软件布局",
                "聊天输入小尾巴",
                "一键锁屏",
                "一键分屏",
                "一键截屏",
                "关机(Root)",
                "重启(Root)",
                "重启到 recovery(Root)",
                "重启到 bootloader(Root)"
            )
    }

}
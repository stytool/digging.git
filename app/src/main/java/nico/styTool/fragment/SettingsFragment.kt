package nico.styTool.fragment

import android.content.Intent
import android.os.Bundle
import android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS
import android.view.View
import androidx.preference.CheckBoxPreference

import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.blankj.utilcode.util.*
import com.blankj.utilcode.util.FileUtils.createOrExistsDir
import com.blankj.utilcode.util.FileUtils.delete
import com.blankj.utilcode.util.PathUtils.getExternalStoragePath
import com.blankj.utilcode.util.PermissionUtils.isGrantedDrawOverlays
import com.blankj.utilcode.util.PermissionUtils.requestDrawOverlays
import com.example.blade.Constant.spName_lay
import com.example.blade.ReaderUtil
import com.example.blade.RxTool
import com.example.blade.util.accessibility.ToolsService
import com.example.blade.util.qq.Android_qq_Share
import com.kongzue.dialogx.dialogs.MessageDialog
import kotlinx.android.synthetic.main.layout_preference.view.*
import nico.styTool.R
import nico.styTool.Utils.getApp

class SettingsFragment : PreferenceFragmentCompat() {
    private val ext = "EnjoyCJZC.ini"
    private val name = getExternalStoragePath()
    private val dest =
        "${name}/Android/data/com.tencent.tmgp.pubgmhd/files/UE4Game/ShadowTrackerExtra/ShadowTrackerExtra/Saved/Config/Android"
    private val path = "${dest}/${ext}"


    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        findPreference<Preference>("donate_alia")?.also {
            donateAlia(it)
        }
        findPreference<Preference>("cli_Stuck")?.also {
            stuck(it)
        }
        findPreference<CheckBoxPreference>("check_box_preference_quality")?.also {
            checkBoxPreferenceQuality(it)
        }
    }

    private fun checkBoxPreferenceQuality(it: CheckBoxPreference) {
        it.setOnPreferenceChangeListener { _, newValue ->
            if (newValue as Boolean) {
                if (createOrExistsDir("${getExternalStoragePath()}/py")) {
                    /*  if (copyFileFromAssets(ext, path)) {
                          //if (FileUtils.isFileExists(path)) {
                              TipDialog.show(
                                  ReaderUtil.baseActivity,
                                  "开启了\n请重启游戏",
                                  TipDialog.TYPE.SUCCESS
                              )
                          //}
                      }*/
                    ToastUtils.showShort("${getExternalStoragePath()}/py")
                }
            } else {
                if (delete(path)) {
                    /*TipDialog.show(
                        BaseBox,
                        "关闭了\n请重启游戏",
                        TipDialog.TYPE.SUCCESS
                    )*/
                }
            }
            true
        }
    }

    private fun donateAlia(it: Preference) {
        it.setOnPreferenceClickListener {
            val url = "https://gitee.com/stytool/software-version/issues/I22M16"
            ReaderUtil.jumpBase(url, null)
            true
        }
    }

    private fun stuck(preference: Preference) {
        preference.setOnPreferenceClickListener {
            val message =
                "卡死了没法跟那个人聊天了？\n『点击 ${getString(android.R.string.cancel)}』\n再一次点进聊天不要翻上去，也可用电脑QQ查以前记录"
            MessageDialog.show(
                "", message,
                getString(android.R.string.ok),
                getString(android.R.string.cancel)
            )
                .setOkButton { _, _ ->
                    val isA = ResourceUtils.readAssets2String("qq_id/a.txt")
                    Android_qq_Share(getApp()).shareQQFriend(
                        null, isA, Android_qq_Share.TEXT, null
                    )
                    false
                }.setCancelButton { _, _ ->
                    val msgText =
                        "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
                    Android_qq_Share(getApp()).shareQQFriend(
                        null, msgText, Android_qq_Share.TEXT, null
                    )
                    false
                }
            true
        }
    }
}
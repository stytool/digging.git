package nico.styTool.fragment

import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat.getColor
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.blankj.utilcode.util.AppUtils.getAppName
import com.blankj.utilcode.util.ResourceUtils.readAssets2List
import com.blankj.utilcode.util.SpanUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.drake.engine.base.EngineFragment
import com.example.blade.ReaderUtil
import com.example.blade.RxTool
import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.BaseFragment
import com.kongzue.baseframework.interfaces.Layout
import com.kongzue.stacklabelview.StackLabel
import com.kongzue.stacklabelview.interfaces.OnLabelClickListener
import kotlinx.android.synthetic.main.fragment_2.view.*
import kotlinx.android.synthetic.main.head_view.view.*
import nico.styTool.R
import java.util.*


class Fragment2 : EngineFragment<ViewDataBinding>(R.layout.fragment_2) {
    override fun initData() {

    }

    override fun initView() {
        requireView().recyclerView2.also {
            it.layoutManager = LinearLayoutManager(requireActivity())
            val adapter =
                object : BaseQuickAdapter<Stack, BaseViewHolder>(R.layout.stack_label, items()) {
                    override fun convert(holder: BaseViewHolder, item: Stack) {
                        holder.apply {
                            setText(R.id.textView7, item.name)
                            getView<StackLabel>(R.id.stackLabel).apply {
                                labels = item.mutableList
                                onLabelClickListener = OnLabelClickListener { _, v, s ->
                                    RxTool.apply {
                                        view = v
                                        activity = requireActivity()
                                        name = s
                                        open()
                                    }
                                }
                            }
                        }
                    }
                }
            it.adapter = adapter.apply {
                val view = layoutInflater.inflate(R.layout.head_view, it, false).apply {
                    this.HeadText.apply {
                        SpanUtils.with(this)
                            /*.append("欢迎使用${getAppName()}").apply {
                            setBold()
                            setForegroundColor(getColorS(R.color.navColorPrimary))
                            append("\n\n")
                        }*/
                            .append(Feedback).apply {
                                setFontSize(12, true)
                            }
                            .append(Feedback2).apply {
                                setFontSize(12, true)
                                setClickSpan(object : ClickableSpan() {
                                    override fun onClick(widget: View) = ReaderUtil.joinQQGroup(key)
                                })
                                append("\n")
                            }
                            .append(Donate).apply {
                                setForegroundColor(
                                    getColor(
                                        requireActivity(),
                                        android.R.color.holo_red_dark
                                    )
                                )
                            }
                            .append(Donate2).apply {
                                setClickSpan(object : ClickableSpan() {
                                    override fun onClick(widget: View) {
                                        BottomRight(requireActivity(), null)
                                            .rxDialog(requireActivity())
                                    }
                                })
                                /*append("\n")*/
                            }
                            .create()
                    }
                }
                addHeaderView(view)
                animationEnable = true
                setAnimationWithDefault(BaseQuickAdapter.AnimationType.ScaleIn)
            }
        }
    }

    private fun items(): MutableList<Stack> {
        val items: MutableList<Stack> = ArrayList()
        layout(items, "日常", 0)
        layout(items, "图片", 1)
        layout(items, "系统", 2)
        layout(items, "游戏", 3)
        layout(items, "开发者工具", 4)
        layout(items, "无障碍工具", 5)
        layout(items, "网页", 6)
        return items
    }

    private fun layout(list: MutableList<Stack>, name: String, int: Int) {
        list.add(Stack(name, assetsData(int)))
    }

    private fun assetsData(int: Int): MutableList<String> =
        readAssets2List("data/${int}.txt")

    companion object {
        private const val load =
            "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1221609048,257947582&fm=26&gp=0.jpg"
        private const val Feedback =
            "目前只开发了十几款小功能，更多实用功能还在持续增加中。要是觉得本应用不错，不妨安利给的 他(她) 人使用，遇到任何问题或建议都能"
        private const val Feedback2 = "反馈及进行留言"
        private const val Donate = "当你觉得应用不错时想捐款,可选择随心意"
        private const val Donate2 = "支持"
        private const val key = "MnWPXV82ZQSaU_id-NQ6DuuzGPPVsYAM"

    }

    data class Stack(
        var name: String?,
        var mutableList: MutableList<String>?,
    )
}
package nico.styTool.fragment

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import cn.vove7.bottomdialog.BottomDialogActivity
import cn.vove7.bottomdialog.builder.mutableList
import cn.vove7.bottomdialog.builder.oneButton
import cn.vove7.bottomdialog.builder.title
import cn.vove7.bottomdialog.interfaces.ContentBuilder
import cn.vove7.bottomdialog.util.ObservableList
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.ToastUtils
import com.example.blade.util.EmptyDialog
import nico.styTool.R
import nico.styTool.bottomDialog.BottomHor
import nico.styTool.builder.AppListBuilder

class BottomRight(requireActivity: FragmentActivity, view: View?) {
    init {
        if (view != null) {
            PopupMenu(requireActivity, view).apply {
                setOnMenuItemClickListener {
                    when (it.title) {
                        array[0] -> BottomHor(requireActivity)
                        array[1] -> ToastUtils.showShort("下次一定")
                        array[2] -> BottomDialogActivity.builder(requireActivity) {
                            title("应用列表")
                            content(AppListBuilder(requireActivity) { _, p, i, l ->
                                EmptyDialog(i.pkg, AppUtils.getAppPath(i.pkg), requireActivity)
                            })
                            oneButton(requireActivity.getString(android.R.string.cancel))
                        }
                    }
                    false
                }
                menu.apply {
                    add(array[0])
                    add(array[1])
                    add(array[2])
                }
                show()
            }
        }
    }

    fun rxDialog(activity: FragmentActivity) {
        BottomDialogActivity.builder(activity) {
            mutableList(list) { _, position, _, _ ->
                when (position) {
                    0 -> {
                        tipBottomDialog(
                            ContextCompat.getDrawable(
                                activity,
                                R.drawable.qq
                            ), activity
                        )
                    }
                    1 -> {
                        tipBottomDialog(
                            ContextCompat.getDrawable(
                                activity,
                                R.drawable.wx
                            ), activity
                        )
                    }
                }
            }
            ToastUtils.showShort("截屏图片再使用扫一扫选择图片")
        }
    }

    private fun tipBottomDialog(drawable: Drawable?, requireActivity: FragmentActivity) {
        BottomDialogActivity.builder(requireActivity) {
            content(object : ContentBuilder() {
                override val layoutRes = R.layout.layout_custom_dialog

                override fun init(view: View) {
                    val imageView = view as ImageView
                    imageView.setImageDrawable(drawable)
                    imageView.setOnClickListener { dialog.dismiss() }
                }

                override fun updateContent(type: Int, data: Any?) = Unit
            })
        }
    }

    companion object {

        private val list = ObservableList.build<String?> {
            add("QQ捐款支持")
            add("微信捐款支持")
        }
        private val array = arrayOf("一句", "一图", "全部应用")
    }
}
package nico.styTool.fragment

import com.blankj.utilcode.util.BarUtils
import com.blankj.utilcode.util.UiMessageUtils
import com.bumptech.glide.Glide
import com.kongzue.baseframework.BaseFragment
import com.kongzue.baseframework.interfaces.Layout
import kotlinx.android.synthetic.main.fragment_1.view.*
import nico.styTool.MainActivity
import nico.styTool.R


@Layout(R.layout.fragment_1)
class Fragment1 : BaseFragment<MainActivity>() {


    override fun initViews() {
        rootView.imageView6.apply {
            Glide.with(requireActivity()).load("https://api.fnmqs.top/api/ip/api.php").into(this)
            BarUtils.addMarginTopEqualStatusBarHeight(this)
        }
        rootView.buttonApps.apply {
            setOnClickListener {

            }
        }

    }

    /* BottomDialogActivity.builder(requireActivity()) {
               title("应用列表")
               content(AppListBuilder(requireActivity()) { _, p, i, l ->
                   toast("$p\n$i\n$l")
               })
               oneButton("取消")
           }*/
    /*  //(0..32).forEach {
      //    println("${it}->{}")
      //}
      if (allAppCount() > 60) {
          MessageDialog.show(BaseBox, "(=ﾟДﾟ=)", "设备应用超过70,请选择条件", "继续加载", "取消加载")
              .setOkButton { _, _ ->
                  ThreadUtils.runOnUiThreadDelayed({ appList() }, 500L)
                  false
              }
              .setCancelButton { _, _ ->
                  me.current()
                  false
              }
      } else {
          ThreadUtils.runOnUiThreadDelayed({
              appList()
          }, 500L)
      }
  }

  private fun appList() {
      rootView.recyclerView.apply {
          BarUtils.addMarginTopEqualStatusBarHeight(this)
          layoutManager = LinearLayoutManager(requireActivity())
          adapter = object :
              BaseQuickAdapter<AppInfo2, BaseViewHolder>(R.layout.item_simple) {
              override fun convert(holder: BaseViewHolder, item: AppInfo2) {
                  holder.also {
                      it.setText(R.id.textView, item.name)
                      if (allAppCount() > 60) {
                          ToastUtils.showShort("软件超过60个暂不显示图片")
                      } else {
                          val imageView = it.getView<ImageView>(R.id.imageView2)
                          val drawable = AppUtils.getAppIcon(item.packageName)
                          GlobalScope.launch(Dispatchers.Main) {
                              delay(1000)
                              Glide.with(context).load(drawable).into(imageView)
                          }
                      }
                  }
              }
          }.apply {
              setDiffCallback(object : DiffUtil.ItemCallback<AppInfo2>() {
                  override fun areContentsTheSame(oldItem: AppInfo2, newItem: AppInfo2) = false
                  override fun areItemsTheSame(oldItem: AppInfo2, newItem: AppInfo2) = false
              })
              GlobalScope.launch { dialogBar(this@apply) }
              setOnItemClickListener { _, _, position ->
                  val adapter1 = this
                  val packageName = adapter1.data[position].packageName.toString()
                  val path = adapter1.data[position].packagePath.toString()
                  EmptyDialog(packageName, path)
              }
              animationEnable = true
              setAnimationWithDefault(BaseQuickAdapter.AnimationType.SlideInLeft)
              val root = rootView.recyclerView.parent
              val view = layoutInflater.inflate(R.layout.top_view_null, root as ViewGroup, false)
              addFooterView(view)
          }
          addOnScrollListener(object : RecyclerView.OnScrollListener() {
              override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                  super.onScrollStateChanged(recyclerView, newState)
                  when (newState) {
                      0 -> {
                          Glide.with(requireActivity()).resumeRequests()
                      }
                      1, 2 -> {
                          Glide.with(requireActivity()).pauseRequests()
                      }
                  }
              }
          })
      }
  }

  private suspend fun dialogBar(param: BaseQuickAdapter<AppInfo2, BaseViewHolder>) {
      val dialog = WaitDialog.show(BaseBox, "列表加载中")
      val result = GlobalScope.async {
          AppUtils2().appsInfo().filter { it.isSystem < true }.toMutableList()
      }
      withContext(Dispatchers.Main) {
          result.await().forEach {
              delay(100)
              dialog.message = "${result.await().indexOf(it) + 1}/${allAppCount()}"
          }
          param.setDiffNewData(result.await())
          WaitDialog.dismiss()
      }
  }
*/
    override fun initDatas() {
        UiMessageUtils.getInstance().addListener(R.id.utilCodeUiMessageAddListenerId) {

        }
    }

    override fun setEvents() {

    }

    override fun onDestroy() {
        super.onDestroy()
        UiMessageUtils.getInstance().removeListeners(R.id.utilCodeUiMessageAddListenerId)
    }
}
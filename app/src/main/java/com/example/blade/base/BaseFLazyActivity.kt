package com.example.blade.base

import android.os.Bundle
import android.view.MenuItem
import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.interfaces.SwipeBack
import nico.styTool.R
import qiu.niorgai.StatusBarCompat

@SwipeBack(true)
abstract class BaseFLazyActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        //onTransformationEndContainer(intent.getParcelableExtra("myTransitionName"))
        super.onCreate(savedInstanceState)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }
        StatusBarCompat.setStatusBarColor(me, me.getColorS(R.color.colorPrimary))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish() // back button
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
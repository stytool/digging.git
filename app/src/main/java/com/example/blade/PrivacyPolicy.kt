package com.example.blade

import android.os.Build
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.FragmentActivity
import cn.vove7.bottomdialog.BottomDialog
import cn.vove7.bottomdialog.builder.buttons
import cn.vove7.bottomdialog.builder.title
import cn.vove7.bottomdialog.interfaces.ContentBuilder
import com.blankj.utilcode.util.AppUtils.getAppName
import com.blankj.utilcode.util.SPUtils
import com.blankj.utilcode.util.SpanUtils
import kotlinx.android.synthetic.main.layout_privacy.view.*
import nico.styTool.HelloActivity
import nico.styTool.R


class PrivacyPolicy(me: HelloActivity) {

    init {
        if (!firstEnterApp()) {
            BottomDialog.builder(me) {
                title("隐私政策", round = true)
                content(object : ContentBuilder() {
                    override val layoutRes = R.layout.layout_privacy

                    override fun init(view: View) {
                        view.textView_privacy.apply {
                            SpanUtils.with(this)
                                .append("感谢您选择${getAppName()}!我们非常重视您的个人信息和隐私保护。")
                                .append("为了更好地保障您的个人权益，在您使用我们的产品前").append("\n")
                                .append("请务必审慎阅读")
                                .append("《隐私政策》")
                                .setClickSpan(object : ClickableSpan() {
                                    override fun onClick(widget: View) {

                                    }

                                    override fun updateDrawState(ds: TextPaint) {
                                        super.updateDrawState(ds)
                                        ds.color = getColor(me, R.color.colorPrimary)
                                        ds.isUnderlineText = false
                                    }
                                })
                                /*.append("和")
                                .append("《用户协议》")
                                .setClickSpan(object : ClickableSpan() {
                                    override fun onClick(widget: View) {

                                    }

                                    override fun updateDrawState(ds: TextPaint) {
                                        super.updateDrawState(ds)
                                        ds.isUnderlineText = false
                                    }

                                })
                                .setForegroundColor(BaseBox.getColorS(R.color.colorPrimary))*/
                                .append("内的所有条款")
                                .append("\n")
                                .append("1. 适用范围：")
                                .append("\n")
                                .append("(a)我们对您的个人信息的不收集/不保存/仅使用显示/保护等规则条款，以及您的用户权利等条款; ")
                                .append("\n")
                                .append("(b)约定我们的限制责任、免责条款;")
                                .append("\n")
                                .append("2. 信息搜集范围：")
                                .append("\n")
                                .append("(a)本应用不涉及记录个人信息，仅浏览器功能记录本机配置信息。")
                                .append("\n")
                                .append("3. 信息存储和交换：")
                                .append("\n")
                                .append("(a)您通过卸载本应用即可删除本应用记录的配置数据。")
                                .append("\n")
                                .append("4.本隐私政策的更改:")
                                .append("\n")
                                .append("(a)如果决定更改隐私政策，我们会在本政策中、本应用中以及我们认为适当的位置发布这些更改，以便您了解我们如何收集、使用您的个人信息，哪些人可以访问这些信息，以及在什么情况下我们会透露这些信息。")
                                .append("\n")
                                .append("(b)本应用保留随时修改本政策的权利，因此请经常查看。如对本政策作出重大更改，本应用会通过通知的形式告知。")
                                .append("\n")
                                .append("(c)其他以颜色或加粗进行标识的重要条款。如您对以上协议有任何疑问，可通过人工客服或发邮件至")
                                .append("2284467793@qq.com")
                                .setForegroundColor(getColor(me, R.color.colorAccent))
                                .append("\n")
                                .append("与我们联系。您点击”同意并继续”的行为即表示您已阅读完毕并同意以上协议的全部内容并开始使用我们的产品和服务!")
                                .create()
                        }
                    }

                    override fun updateContent(type: Int, data: Any?) {

                    }

                }).apply {
                    mCancelable = false
                    navBgColor = ContextCompat.getColor(me, R.color.navColorPrimary)
                }

                buttons {
                    negativeButton("暂不使用") {
                        me.finishTransition()
                    }
                    positiveButton("同意并继续") {
                        it.dismiss()
                        SPUtils.getInstance().put("SP_IS_FIRST_ENTER_APP", true)
                        RxShow(me)
                    }
                }
            }.setCancelable(false)
            /*
            MessageDialog.show(BaseBox, null, "隐私政策", "同意并继续", "暂不使用")
                .setCancelable(false)
                .setCustomView(R.layout.layout_privacy) { _, v -> //绑定布局事件，可使用v.findViewById(...)来获取子组件
                    v.textView_privacy.apply {
                        SpanUtils.with(this)
                            .append("感谢您选择${getAppName()}!我们非常重视您的个人信息和隐私保护。")
                            .append("为了更好地保障您的个人权益，在您使用我们的产品前").append("\n")
                            .append("请务必审慎阅读")
                            .append("《隐私政策》")
                            .setClickSpan(object : ClickableSpan() {
                                override fun onClick(widget: View) {

                                }

                                override fun updateDrawState(ds: TextPaint) {
                                    super.updateDrawState(ds)
                                    ds.color = BaseBox.getColorS(R.color.colorPrimary)
                                    ds.isUnderlineText = false
                                }
                            })
                            /*.append("和")
                            .append("《用户协议》")
                            .setClickSpan(object : ClickableSpan() {
                                override fun onClick(widget: View) {

                                }

                                override fun updateDrawState(ds: TextPaint) {
                                    super.updateDrawState(ds)
                                    ds.isUnderlineText = false
                                }

                            })
                            .setForegroundColor(BaseBox.getColorS(R.color.colorPrimary))*/
                            .append("内的所有条款")
                            .append("\n")
                            .append("1. 适用范围：")
                            .append("\n")
                            .append("(a)我们对您的个人信息的不收集/不保存/仅使用显示/保护等规则条款，以及您的用户权利等条款; ")
                            .append("\n")
                            .append("(b)约定我们的限制责任、免责条款;")
                            .append("\n")
                            .append("2. 信息搜集范围：")
                            .append("\n")
                            .append("(a)本应用不涉及记录个人信息，仅浏览器功能记录本机配置信息。")
                            .append("\n")
                            .append("3. 信息存储和交换：")
                            .append("\n")
                            .append("(a)您通过卸载本应用即可删除本应用记录的配置数据。")
                            .append("\n")
                            .append("4.本隐私政策的更改:")
                            .append("\n")
                            .append("(a)如果决定更改隐私政策，我们会在本政策中、本应用中以及我们认为适当的位置发布这些更改，以便您了解我们如何收集、使用您的个人信息，哪些人可以访问这些信息，以及在什么情况下我们会透露这些信息。")
                            .append("\n")
                            .append("(b)本应用保留随时修改本政策的权利，因此请经常查看。如对本政策作出重大更改，本应用会通过通知的形式告知。")
                            .append("\n")
                            .append("(c)其他以颜色或加粗进行标识的重要条款。如您对以上协议有任何疑问，可通过人工客服或发邮件至")
                            .append("2284467793@qq.com")
                            .setForegroundColor(BaseBox.getColorS(R.color.colorAccent))
                            .append("\n")
                            .append("与我们联系。您点击”同意并继续”的行为即表示您已阅读完毕并同意以上协议的全部内容并开始使用我们的产品和服务!")
                            .create()
                    }

                }
                .setOkButton { _, _ ->
                    SPUtils.getInstance().put("SP_IS_FIRST_ENTER_APP", true)
                    false
                }
                .setCancelButton { _, _ ->
                    handleFinish()
                    false
                }*/
        }

    }


    private fun firstEnterApp() = SPUtils.getInstance().getBoolean("SP_IS_FIRST_ENTER_APP", false)


    companion object {
        private const val high_light_1 = "《用户协议》"
        private const val high_light_2 = "《隐私政策》"
        private const val isFirstUse = false
    }
}
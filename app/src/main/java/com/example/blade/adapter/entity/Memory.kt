package com.example.blade.adapter.entity

import androidx.annotation.Keep

@Keep
data class Memory(
    val ramMemoryAvailable: String,
    val ramMemoryTotal: String,
    val romMemoryAvailable: String,
    val romMemoryTotal: String,
    val sdCardMemoryAvailable: String,
    val sdCardMemoryTotal: String,
    val sdCardRealMemoryTotal: String
)
package com.example.blade.adapter.entity

import androidx.annotation.Keep

@Keep
data class Battery(
        var br: String,
        var status: String,
        var plugState: String,
        var health: String,
        var present: String,
        var technology: String,
        var temperature: String,
        var voltage: String,
        var power: String
)
package com.example.blade.app

data class AppInfo2(
    var packageName: String,
    var name: String,
    var packagePath: String,
    var isSystem: Boolean
)
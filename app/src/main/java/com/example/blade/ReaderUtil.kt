package com.example.blade

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.view.View
import androidx.annotation.ColorInt
import cn.vove7.andro_accessibility_api.AccessibilityApi
import com.blankj.utilcode.util.*
import com.example.blade.activity.browser.ActiveWeb
import com.example.blade.activity.browser.temporary.WebViewActivity
import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.util.AppManager
import com.kongzue.baseframework.util.JumpParameter
import nico.styTool.Utils.getApp


/**
 * @author Nico
 */
object ReaderUtil {


    /* var BaseBox: BaseActivity = AppManager.getInstance().currentActivity()*/

    fun jumpBase(name: String, view: View?) {
        if (name != "") {
            SPUtils.getInstance(Constant.spName).put("AcceptData", name).apply {
                ActivityUtils.startActivity(WebViewActivity::class.java)
            }
            //if (!BaseBox.jump(ActiveWeb::class.java, JumpParameter().put("name", name), view)) {
            //   BaseBox.jump(ActiveWeb::class.java, JumpParameter().put("name", name))
            //}
        }

    }

    fun jExit(accessibilityApi: AccessibilityApi, exit: Boolean) {
        ThreadUtils.runOnUiThread {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                accessibilityApi.disableSelf()
            }
            ProcessUtils.killBackgroundProcesses(accessibilityApi.packageName)
            if (exit) {
                AppManager.getInstance().exit(accessibilityApi)
            }
        }
    }

    fun joinQQGroup(key: String) {
        val intent = Intent()
        intent.data = Uri.parse(uri(key))
        try {
            getApp().startActivity(intent)
        } catch (e: Exception) {
            ToastUtils.showShort("未安装手Q或安装的版本不支持")
        }

    }

    @SuppressLint("QueryPermissionsNeeded")
    fun allAppCount() =
        getApp().packageManager.getInstalledPackages(0)
            .filter { ApplicationInfo.FLAG_SYSTEM and it.applicationInfo.flags == 0 }.size


    /* // Q_processing_tools, "全部网址功能", "过渡动画修改", "电池伪装" -> isis2(mActivity, tvq)'
     @JvmStatic
     fun execute(tvq: String?) {
         when (tvq) {


             //"YY1" -> isis(mActivity, "https://code.aliyun.com/open-Coed/Ni-Coed/blob/master/videogame_asset.md")

             //"YY2" -> isis(mActivity, "https://code.aliyun.com/open-Coed/Ni-Coed/blob/master/videogame_hospital.md")

             //"YY3" -> isis(mActivity, "https://code.aliyun.com/open-Coed/Ni-Coed/blob/master/videogame_map.md")


             else -> {
             }
         }
     }*/

    private fun uri(key: String) =
        "mqqopensdkapi://bizAgent/qm/qr?url=http%3A%2F%2Fqm.qq.com%2Fcgi-bin%2Fqm%2Fqr%3Ffrom%3Dapp%26p%3Dandroid%26k%3D$key"

    fun isLightColor(@ColorInt color: Int): Boolean {
        return 0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color) >= 127.5
    }
}
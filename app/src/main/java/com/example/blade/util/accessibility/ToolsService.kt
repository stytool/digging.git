package com.example.blade.util.accessibility

import android.view.accessibility.AccessibilityEvent
import cn.vove7.andro_accessibility_api.AccessibilityApi
import cn.vove7.andro_accessibility_api.AppScope
import com.blankj.utilcode.util.SPUtils
import com.example.blade.Constant.spName_lay
import com.lzf.easyfloat.EasyFloat.Companion.dismissAppFloat


class ToolsService : AccessibilityApi() {

    /*val event0: AccessibilityEvent? = null*/
    override val enableListenAppScope: Boolean = false
     val accessibilityNodeInfo by lazy { rootInWindow }


    override fun onCreate() {
        baseService = this
        super.onCreate()
    }

    override fun onDestroy() {
        dismissAppFloat("$this")
        baseService = null
        super.onDestroy()
    }

    override fun onPageUpdate(currentScope: AppScope) = Unit

    override fun onServiceConnected() {
        requireBaseAccessibility()
        /* if (spUtils(0)) {
             QQBasicService().tools(this)
         }
         if (spUtils(1)) {
             AnalysisService()
         }
         if (spUtils(2)) {
             TailService().tailAccessibilityService(this)
         }
         if (spUtils(3)) {
             ScreenshotService().screenshotAccessibilityService(this)
         }*/
    }

   /* override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        //super.onAccessibilityEvent(event)
        event0 = event
    }
*/
    private fun spUtils(key: Int) =
        SPUtils.getInstance(spName_lay).getBoolean(key.toString(), false)

}
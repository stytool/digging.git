package com.example.blade.util.color;

import android.graphics.Bitmap;

public class ImageProcessUtils {

    private static int[] pStore;
    private static int[] sOriginal;

    public static Bitmap invertBitmap(Bitmap bitmap) {
        //width
        int sWidth = bitmap.getWidth();
        //height
        int sHeight = bitmap.getHeight();
        int[] sPixels = new int[sWidth * sHeight];
        bitmap.getPixels(sPixels, 0, sWidth, 0, 0, sWidth, sHeight);

        int sIndex = 0;
        //Row--height
        int sRow;
        for (sRow = 0; sRow < sHeight; sRow++) {
            sIndex = sRow * sWidth;
            //col--width
            int sCol;
            for (sCol = 0; sCol < sWidth; sCol++) {
                int sPixel = sPixels[sIndex];
                //ARGB values
                int sA = (sPixel >> 24) & 0xff;
                int sR = (sPixel >> 16) & 0xff;
                int sG = (sPixel >> 8) & 0xff;
                int sB = sPixel & 0xff;

                sR = 255 - sR;
                sG = 255 - sG;
                sB = 255 - sB;

                sPixel = ((sA & 0xff) << 24 | (sR & 0xff) << 16 | (sG & 0xff) << 8 | sB & 0xff);

                sPixels[sIndex] = sPixel;

                sIndex++;
            }
        }
        bitmap.setPixels(sPixels, 0, sWidth, 0, 0, sWidth, sHeight);
        return bitmap;
    }

    public static int invertBitmap(int bitmap) {
        int sIndex;
        int sPixel = bitmap;
        int sA = (sPixel >> 24) & 0xff;
        int sR = (sPixel >> 16) & 0xff;
        int sG = (sPixel >> 8) & 0xff;
        int sB = sPixel & 0xff;
        sR = 255 - sR;
        sG = 255 - sG;
        sB = 255 - sB;
        sPixel = ((sA & 0xff) << 24 | (sR & 0xff) << 16 | (sG & 0xff) << 8 | sB & 0xff);
        sIndex = sPixel;
        return sIndex;
    }


}

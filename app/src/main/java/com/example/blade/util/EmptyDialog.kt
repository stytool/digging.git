package com.example.blade.util

import android.graphics.Bitmap
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.util.*
import com.blankj.utilcode.util.AppUtils.*
import com.drake.net.utils.withMain
import com.kongzue.dialogx.dialogs.BottomDialog
import com.kongzue.dialogx.dialogs.BottomMenu
import com.kongzue.dialogx.dialogs.TipDialog
import com.kongzue.dialogx.dialogs.WaitDialog
import com.kongzue.dialogx.interfaces.OnBindView
import com.kongzue.dialogx.interfaces.OnIconChangeCallBack
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import nico.styTool.R


/*
这么优美的代码，为啥需要优化
 */
class EmptyDialog(packageName: String, path: String, requireActivity: FragmentActivity) {
    private val file = PathUtils.getExternalAppPicturesPath()
    private val src = ImageUtils.drawable2Bitmap(getAppIcon(packageName))
    private val filePath = file + "/" + ColorUtils.getRandomColor() + ".png"
    private val format = Bitmap.CompressFormat.PNG
    private val dest = PathUtils.getExternalAppDownloadPath() + "/${packageName}.apk"

    init {
        val message = "path: $path"
        ThreadUtils.runOnUiThreadDelayed({
            BottomMenu.show(list)
                .setMessage("packageName: ${packageName}\n$message")
                .setOnIconChangeCallBack(object : OnIconChangeCallBack() {
                    override fun getIcon(a: BottomMenu, i: Int, m: String): Int {
                        when (i) {
                            0 -> return R.drawable.ic_baseline_menu_open_24
                            1 -> return R.drawable.ic_baseline_delete_forever_24
                            2 -> return R.drawable.ic_baseline_settings_applications_24
                            3 -> return R.drawable.ic_baseline_get_app_24
                            4 -> return R.drawable.ic_baseline_file_copy_24
                        }
                        return 0
                    }
                })
                .setOnMenuItemClickListener { _, _, index ->
                    when (index) {
                        0 -> launchApp(packageName)
                        1 -> uninstallApp(packageName)
                        2 -> launchAppDetailsSettings(packageName)
                        3 -> {
                            WaitDialog.show("")
                            GlobalScope.launch(Dispatchers.IO) {
                                if (FileUtils.copy(path, dest)) {
                                    withMain {
                                        TipDialog.show(dest, WaitDialog.TYPE.SUCCESS)
                                        ToastUtils.showShort("已提取到:$dest")
                                    }
                                }
                            }
                        }
                        4 -> {
                            val isBoolean = ImageUtils.save(src, filePath, format)
                            return@setOnMenuItemClickListener if (isBoolean) {
                                nico.styTool.Utils.setToast("已保存至:$filePath")
                                FileUtils.notifySystemToScan(filePath)
                                false
                            } else {
                                nico.styTool.Utils.setToast("保存失败，请联系开发者")
                                true
                            }
                        }
                    }
                    false
                }
            /*.setCustomView(object : OnBindView<BottomDialog>(R.layout.layout_custom_view) {
                override fun onBind(dialog: BottomDialog, v: View) {

                }
            })*/
        }, 500L)
        /*MessageDialog.show(requireActivity, null, null, "更多信息")
            .setCustomView(R.layout.software_dialog) { _, v ->
                SpanUtils
                    .with(v.textView6)
                    .append(row)
                    .append("软件名").setBold()
                    .append(row)
                    .append(getAppName(packageName))
                    .append(row)
                    .append(row)
                    .append("包名").setBold()
                    .append(row)
                    .append(packageName)
                    .append(row)
                    .append(row)
                    .append("版本号").setBold()
                    .append(row)
                    .append(getAppVersionCode(packageName).toString())
                    .append(row)
                    .append(row)
                    .append("安装包大小").setBold()
                    .append(row)
                    .append(getSize(path))
                    .append(row)
                    .append(row)
                    .append("路径").setBold()
                    .append(row)
                    .append(path)
                    .append(row)
                    .append(row)
                    .append("UID").setBold()
                    .append(row)
                    .append(getAppUid(packageName).toString()).create()
                v.imageView_icon.setImageDrawable(getAppIcon(packageName))

            }.setOkButton { _, _ ->
                BottomMenu.show(BaseBox, list) { _, index ->
                    when (index) {
                        0 -> {
                            launchApp(packageName)
                        }
                        1 -> {
                            uninstallApp(packageName)
                        }
                        2 -> {
                            launchAppDetailsSettings(packageName)
                        }
                        3 -> {
                            WaitDialog.show(BaseBox, "")
                            GlobalScope.launch(Dispatchers.IO) {
                                if (FileUtils.copy(path, dest)) {
                                    withMain {
                                        TipDialog.show(BaseBox, dest, TipDialog.TYPE.SUCCESS)
                                        ToastUtils.showShort("已提取到:$dest")
                                    }
                                }
                            }
                        }
                        4 -> {
                            return@show if (ImageUtils.save(src, filePath, format)) {
                                nico.styTool.Utils.setToast("壁纸已保存至:$filePath")
                                FileUtils.notifySystemToScan(filePath)
                            } else {
                                nico.styTool.Utils.setToast("壁纸保存失败，请联系开发者")

                            }
                        }
                    }
                }
                return@setOkButton false

            }*/
    }


    companion object {
        private const val row = "\n"
        private val list = listOf("打开软件", "卸载软件", "应用信息", "提取软件", "提取软件图标")
    }
}
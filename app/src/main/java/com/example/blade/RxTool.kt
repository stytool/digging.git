package com.example.blade

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.blankj.utilcode.constant.RegexConstants
import com.blankj.utilcode.util.ActivityUtils
import com.blankj.utilcode.util.RegexUtils
import com.blankj.utilcode.util.ToastUtils
import com.example.blade.ReaderUtil.jumpBase
import com.example.blade.activity.device.DeviceFLazyActivity
import com.example.blade.activity.like.LikeActivity
import com.example.blade.activity.small_font.MeiFLazyActivity
import com.example.blade.activity.tailoring.bin_FLazy_Activity
import com.example.blade.activity.text.Special_textFLazyActivity
import com.example.blade.util.shell.MyShell
import me.jingbin.web.ByWebTools
import nico.styTool.Utils.setBottomMenu
import nico.styTool.click.*

class RxTool {
    companion object {
        internal lateinit var view: View
        internal lateinit var activity: FragmentActivity
        internal lateinit var name: String
        internal fun open() {
            when (name) {
                "MiKuTools" -> jumpBase("https://tools.miku.ac/", view)
                "实时汇率换算" -> jumpBase("https://tools.miku.ac/exchange_rate", view)
                "小字体数字/蓝色英文字" -> ActivityUtils.startActivity(MeiFLazyActivity::class.java)
                "显卡天梯图" -> jumpBase("https://tools.miku.ac/gpu_rank", view)
                "悬浮时间" -> DailyTools().time(activity)
                "悬浮电量" -> DailyTools().power(activity)
                "悬浮文本" -> {

                }
                "特殊字符生成" -> ActivityUtils.startActivity(Special_textFLazyActivity::class.java)
                "新型肺炎疫情实时动态" -> {
                    "https://code.aliyun.com/open-Coed/Ni-Coed/blob/master/videogame_hospital.md".apply {
                        jumpBase(this, view)
                    }
                }
                "哔哩哔哩封面提取" -> jumpBase("http://b.qiuyeye.cn/index.html", view)
                "INStAGram风格生成器" -> jumpBase("https://igfonts.io/", view)
                "获取当前壁纸" -> PictureTool(activity)
                "图片裁剪形状+蓝色英文字" -> ActivityUtils.startActivity(bin_FLazy_Activity::class.java)
                "手机参数信息" -> ActivityUtils.startActivity(DeviceFLazyActivity::class.java)
                "电量伪装" -> SystemTool(activity)
                "强制隐藏导航栏和状态栏*" -> {
                }
                "去除正在其他应用上层显示" -> {
                    Intent("android.settings.APP_NOTIFICATION_SETTINGS").apply {
                        putExtra("android.provider.extra.APP_PACKAGE", "android")
                        try {
                            ActivityUtils.startActivity(activity, this)
                        } catch (e: Exception) {
                            nico.styTool.Utils.setToast("e: Exception")
                        }
                    }
                }
                "执行SU" -> {
                    InputBox.Builder(activity, Hint = arrayOf("执行SU"))
                        .setPositiveButton {
                            MyShell().shell(InputBox.SaveData)
                        }.create()
                }
                "王者荣耀重名" -> GameTools().kinna()
                "和平精英极限画质工具" -> ActivityUtils.startActivity(LikeActivity::class.java)
                "王者荣耀国标" -> GameTools().wzMark()
                "中心标" -> GameTools().mark(activity)
                "在家无聊?点我免费玩一年(无需下载)" -> {
                    "https://code.aliyun.com/open-Coed/Ni-Coed/blob/master/videogame_asset.md".apply {
                        jumpBase(this, view)
                    }
                }
                "网页图片采集" -> DeveloperTools(activity)
                "正则工具" -> {

                    val list = arrayOf("在线工具", "站长工具", "菜鸟工具")
                    setBottomMenu(menuList = list, callback = {
                        when (it) {
                            0 -> jumpBase("https://tool.lu/regex/", view)
                            1 -> jumpBase("http://tool.chinaz.com/regex/", view)
                            2 -> jumpBase("https://c.runoob.com/front-end/854", view)
                        }
                    })
                }
                "QQ名片自动回赞", "QQ消息连发器", "输入框增加小尾巴", "QQ额外辅助" -> {
                    ActivityUtils.startActivity(LikeActivity::class.java)
                }
                "QQ强制聊天" -> {
                    InputBox.Builder(activity, Hint = arrayOf(qqNum))
                        .setPositiveButton {
                            val regex = RegexConstants.REGEX_QQ_NUM
                            if (RegexUtils.isMatch(regex, InputBox.SaveData)) {
                                activity.startActivity(Intent().apply {
                                    data = Uri.parse("${mandatory}${InputBox.SaveData}")
                                })
                                it.dismiss()
                            }
                        }.create()
                }
                "2000套电视直播在线观看" -> jumpBase("http://m.haoqu.net/", view)
                "搜（情头）配对" -> jumpBase(
                    "https://m.duitang.com/album/?id=89463087&start=0&limit=24",
                    view
                )
                "加密/解密" -> jumpBase("https://www.sojson.com/encrypt.html", view)
                "APK反编译" -> jumpBase("https://www.toolfk.com/tool-decompile-apk", view)
                "鬼畜字符生成器" -> jumpBase("http://tool.mkblog.cn/guichu/", view)
                "花样撤回消息" -> jumpBase("http://tool.mkblog.cn/qqche/", view)
                "好友克隆" -> jumpBase("http://vip.qq.com/client/fr_index.html", view)
                "防沉迷游戏解封" -> jumpBase("http://gamesafe.qq.com/comm_auth.shtml", view)
                "ConVerTio-文件转换器" -> jumpBase("https://convertio.co/zh/", view)
                "office-文件转换器" -> jumpBase("http://cn.office-converter.com/", view)
                "图片放大" -> jumpBase("http://bigjpg.com/", view)
                "tiny压缩图片" -> jumpBase("https://tinypng.com/", view)
                "在线文本比较" -> jumpBase("http://wenbenbijiao.renrensousuo.com/", view)
                "手写体在线转换" -> jumpBase("http://www.qqxiuzi.cn/zh/shouxie-shufa/", view)
                "星座屋-(星座运势)" -> jumpBase("https://m.xzw.com/", view)
                "MIui国际" -> jumpBase("http://en.miui.com/getrom.php?m=yes", view)
                "MIui官方" -> jumpBase("http://www.miui.com/getrom.php?m=yes", view)
                "MIui意大利" -> jumpBase("http://www.miui.it/#", view)
                "MIui俄罗斯" -> jumpBase("https://miui.su/download", view)
                "MIui希腊" -> {
                    "https://xiaomi-miui.gr/community/wsif/index.php/CategoryList/?s=22ae3404fd063af2312f867fbecc84df8fc1eab1".apply {
                        jumpBase(this, view)
                    }
                }
                "MIui波兰" -> jumpBase("https://miuipolska.pl/download/", view)
                "进入自定义网址" -> {
                    InputBox.Builder(activity, Hint = arrayOf("搜索戓进入网址"))
                        .setPositiveButton {
                            jumpBase(ByWebTools.getUrl(InputBox.SaveData), view)
                            it.dismiss()
                        }.create()
                }
                else -> println(0)
            }
        }

        private const val mandatory = "mqqwpa://im/chat?chat_type=wpa&uin="
        private const val qqNum = "QQ号"
    }

}

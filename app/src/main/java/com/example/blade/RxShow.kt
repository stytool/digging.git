package com.example.blade

import androidx.fragment.app.FragmentActivity
import cn.vove7.bottomdialog.BottomDialog
import cn.vove7.bottomdialog.builder.buttons
import cn.vove7.bottomdialog.builder.message
import cn.vove7.bottomdialog.builder.title
import com.blankj.utilcode.util.AppUtils.getAppVersionName
import com.blankj.utilcode.util.AppUtils.uninstallApp
import com.blankj.utilcode.util.SPUtils
import nico.styTool.fragment.BottomRight

class RxShow(requireActivity: FragmentActivity) {
    init {
        if (show()) {
            if (!firstEnterApp()) {
                BottomDialog.builder(requireActivity) {
                    title("Hello")
                    message(string, selectable = true)
                    buttons {
                        positiveButton("支持") {
                            it.dismiss()
                            BottomRight(requireActivity, null).rxDialog(requireActivity)
                        }
                        negativeButton("卸载") {
                            //it.dismiss()
                            uninstallApp(requireActivity.packageName)
                        }
                        neutralButton("") {
                            BottomRight(requireActivity, null).rxDialog(requireActivity)
                            SPUtils.getInstance().put(key, true)
                        }
                    }
                }
            }
        }
    }

    private fun firstEnterApp() = SPUtils.getInstance().getBoolean(key, false)
    private fun show() = false

    companion object {
        private const val string =
            "要不要支持下该软件作者?\n虽然一直弹出是个不好现象\n还不能(不再提示) 绝望不 别多想,也许我生活比你更绝望\n\n#但是您可以选择自己编译该软件源码\n链接：https://gitee.com/stytool/digging.git"
        private val key = "SP_IS_Show_APP_${getAppVersionName()}"
    }
}
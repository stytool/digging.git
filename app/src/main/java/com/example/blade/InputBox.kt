package com.example.blade

import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.FragmentActivity
import cn.vove7.bottomdialog.BottomDialog
import cn.vove7.bottomdialog.BottomDialogActivity
import cn.vove7.bottomdialog.builder.OnClick
import cn.vove7.bottomdialog.builder.buttons
import cn.vove7.bottomdialog.interfaces.ContentBuilder
import kotlinx.android.synthetic.main.search_input.view.*
import nico.styTool.R

class InputBox private constructor() {

    class Builder(
        private val context: FragmentActivity,
        private val off: Boolean = false,
        private val Hint: Array<String>
    ) {

        private lateinit var positiveButtonContent: CharSequence
        private lateinit var negativeButtonContent: CharSequence
        private var positiveButtonListener: OnClick? = null
        private var negativeButtonListener: OnClick? = null

        fun setPositiveButton(
            text: CharSequence = context.getString(android.R.string.ok),
            listener: (dialog: BottomDialog) -> Unit
        ): Builder {
            this.positiveButtonContent = text
            this.positiveButtonListener = listener
            return this
        }

        fun setNegativeButton(
            text: CharSequence = context.getString(android.R.string.cancel),
            listener: (dialog: BottomDialog) -> Unit
        ): Builder {
            this.negativeButtonContent = text
            this.negativeButtonListener = listener
            return this
        }

        fun create() {
            BottomDialogActivity.builder(context) {
                content(object : ContentBuilder() {
                    override val layoutRes = R.layout.search_input
                    override fun init(view: View) {
                        view.apply {
                            textInputLayout.hint = Hint[0]
                            if (Hint.size > 1) textInputLayout2.hint = Hint[1]
                            if (!off) {
                                textView.visibility = View.GONE
                                textInputLayout2.visibility = View.GONE
                            }
                            _input.doAfterTextChanged { SaveData = it.toString() }
                            _input2.doAfterTextChanged { Save2Data = it.toString() }
                        }
                    }

                    override fun updateContent(type: Int, data: Any?) = Unit
                })
                buttons {
                    positiveButtonListener?.let { positiveButton(positiveButtonContent, it) }
                    negativeButtonListener?.let { negativeButton(negativeButtonContent, it) }

                }
            }
        }

    }

    companion object {
        var SaveData: String = ""
        var Save2Data: String = ""
    }
}

package com.example.blade.activity.browser

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.webkit.WebView
import com.blankj.utilcode.util.SnackbarUtils
import me.jingbin.web.BuildConfig

class WebTools {
    fun handleThirdApp(activity: Activity, url: String, webView: WebView): Boolean =
        if (url.startsWith("http") || url.startsWith("https") || url.startsWith("ftp")) {
            println(0)
            false
        } else {
            startActivity(activity, url, webView)
            true
        }

    private fun startActivity(context: Context, url: String, webView: WebView) {
        SnackbarUtils.with(webView).setMessage("允许网页启动其它应用?").setAction("允许") {
            webView.goBack()
            val intent = Intent()
            intent.action = "android.intent.action.VIEW"
            intent.data = Uri.parse(url)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            try {
                context.startActivity(intent)
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }
        }.show()
    }
}
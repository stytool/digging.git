package com.example.blade.activity;

import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.blankj.utilcode.util.ScreenUtils;
import com.ramotion.paperonboarding.PaperOnboardingEngine;
import com.ramotion.paperonboarding.PaperOnboardingPage;

import java.util.ArrayList;

import nico.styTool.R;

public class PaperOnActivity extends AppCompatActivity {

    private static void onPageChanged(int oldElementIndex, int newElementIndex) {
    }

    private static void onLeftOut() {
    }

    private static void onRightOut() {
        /*
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment bf = new BlankFragment();
        fragmentTransaction.replace(R.id.fragment_container, bf);
        fragmentTransaction.commit();
         */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.onboarding_main_layout);
        ScreenUtils.setFullScreen(this);
        ActionBar ActionBar = getSupportActionBar();
        if (ActionBar != null) {
            ActionBar.hide();
        }
        PaperOnboardingEngine engine = new PaperOnboardingEngine(findViewById(R.id.onboardingRootView), getDataForOaring(), getApplicationContext());
        engine.setOnChangeListener(PaperOnActivity::onPageChanged);
        engine.setOnLeftOutListener(PaperOnActivity::onLeftOut);
        engine.setOnRightOutListener(PaperOnActivity::onRightOut);

    }


    private ArrayList<PaperOnboardingPage> getDataForOaring() {
        // prepare data
        PaperOnboardingPage scr1 = new PaperOnboardingPage("Hotels", "All hotels and hostels are sorted by hospitality rating", Color.parseColor("#678FB4"), R.drawable.hotels, R.drawable.key);
        PaperOnboardingPage scr2 = new PaperOnboardingPage("Banks", "We carefully verify all banks before add them into the app", Color.parseColor("#65B0B4"), R.drawable.banks, R.drawable.wallet);
        PaperOnboardingPage scr3 = new PaperOnboardingPage("Stores", "All local stores are categorized for your convenience", Color.parseColor("#9B90BC"), R.drawable.stores, R.drawable.shopping_cart);

        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
        elements.add(scr1);
        elements.add(scr2);
        elements.add(scr3);
        return elements;
    }
}
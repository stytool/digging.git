package com.example.blade.activity.browser

import com.blankj.utilcode.util.ActivityUtils
import com.example.blade.activity.browser.temporary.WebViewActivity
import com.kongzue.baseframework.BaseActivity
import com.kongzue.baseframework.interfaces.Layout
import com.kongzue.baseframework.util.JumpParameter
import me.jingbin.web.ByWebView
import nico.styTool.R


@Layout(R.layout.activity_activit_web)
class ActiveWeb : BaseActivity() {
    //private lateinit var byWebView: ByWebView
    override fun initViews() {
        //ActivityUtils.startActivity(WebViewActivity::class.java)
        /*//setSupportActionBar(bottom_app_bar)
        bottom_app_bar.apply {
            setOnMenuItemClickListener {
                PointIcon(byWebView.webView, this@ActiveWeb, it.itemId)
                false
            }
        }

        val jump = parameter["name"].toString()
        val parameter = if (jump == "") "https://baidu.com" else jump

        byWebView = ByWebView
            .with(this)
            .setWebParent(web_view, FrameLayout.LayoutParams(-1, -1))
            .setCustomWebView(NestedScrollWebView(this))
            .useWebProgress(false)
            .setOnTitleProgressCallback(onTitleProgressCallback)
            .setOnByWebClientCallback(onByWebClientCallback)
            .setErrorLayout(R.layout.by_load_url_error, "无法打开网页")
            .loadUrl(parameter).apply {
                ThreadUtils.runOnUiThread {
                    if (SPUtils.getInstance(spName).contains(restoreUrl)) {
                        SnackbarUtils.with(this.webView).setMessage("恢复上次浏览?").setAction("恢复") {
                            val url = SPUtils.getInstance(spName).getString(restoreUrl)
                            if (url != "") {
                                webView.loadUrl(url)
                            }
                        }.show()
                    }
                }
                webView.apply {
                    *//*webChromeClient = object : ByWebChromeClient(this@ActiveWeb,byWebView) {
                        override fun onProgressChanged(view: WebView?, newProgress: Int) {
                            super.onProgressChanged(view, newProgress)
                            if (view != null) {
                                synchronized(view.url) {
                                    println("重写这狗日东西")
                                    Log.d("TAG520", view.url)
                                }
                            }
                        }
                    }*//*
                }
            }
        */
    }


    /*  private val onTitleProgressCallback = object : OnTitleProgressCallback() {
          override fun onReceivedTitle(title: String) {

          }

          override fun onProgressChanged(newProgress: Int) {}
      }

      private val onByWebClientCallback = object : OnByWebClientCallback() {

          override fun onPageFinished(view: WebView, url: String) {

          }

          override fun isOpenThirdApp(url: String): Boolean {
              //println(url)
              return WebTools().handleThirdApp(this@ActiveWeb, url, byWebView.webView)
          }
      }*/


    override fun initDatas(parameter: JumpParameter?) = Unit

    override fun setEvents() {

    }

}
/* private lateinit var mWebView: ScrollWebView

 private var x5WebChromeClient: X5WebChromeClient? = null


 override fun initViews() {
     //supportActionBar?.also {
     //   it.hide()
     //}
     AndroidBug5497Workaround.assistActivity(me)
     mWebView = web_view.also {
         //addMarginTopEqualStatusBarHeight(it)
         it.setOpenLayerType(true)
         it.setSavePassword(true)
         x5WebChromeClient = it.x5WebChromeClient
         val webViewClient = YcX5WebViewClient(it, this)
         it.webViewClient = webViewClient
         it.x5WebChromeClient.setWebListener(interWebListener)
         it.x5WebViewClient.setWebListener(interWebListener)
     }

 }

 override fun initDatas(parameter: JumpParameter) {
     val parameter1 = parameter["name"] as String
     if (isNull(parameter1)) handleFinish() else mWebView.loadUrl(parameter1)
 }

 override fun setEvents() {
     mWebView.setOnLongClickListener { handleLongImage() }
     if (SPUtils.getInstance().getBoolean(parameter["name"].toString(), true)) {
         MessageDialog.show(
             me,
             "提示",
             "这是一个浏览器功能，这里没有一个官方网址\n\n如遇到第三方相关收费顶目，请自己鉴定是否使用！",
             "同意",
             "不同意"
         )
             .setOkButton { _, _ ->
                 SPUtils.getInstance().put(parameter["name"].toString(), false)
                 false
             }
     }
 }

 private var mExitTime: Long = 0

 override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
     if (keyCode == KeyEvent.KEYCODE_BACK) {
         if (x5WebChromeClient != null && x5WebChromeClient!!.inCustomView()) {
             x5WebChromeClient!!.hideCustomView()
             return true
         } else if (mWebView.pageCanGoBack()) {
             return mWebView.pageGoBack()
         } else {
             if (System.currentTimeMillis() - mExitTime > 2000) {
                 nico.styTool.Utils.setToast("再按一次退出")
                 mExitTime = System.currentTimeMillis()
             } else {
                 handleFinish()
             }
         }
     }
     return false
 }

 private fun handleFinish() {
     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
         finishAfterTransition()
     } else {
         finishActivity()
     }
 }

 private val interWebListener: InterWebListener = object : InterWebListener {
     override fun hindProgressBar() {
         //pb.setVisibility(View.GONE);
         //statusBarAerie?.hideIndeterminateProgress()
     }

     override fun showErrorView(type: Int) {
     }

     override fun startProgress(newProgress: Int) {
         when (newProgress) {
             100 -> {
                 val bitmap = view2Bitmap(mWebView)
                 val pixel = bitmap.getPixel(3, 3)
                 setStatusBarColor(me, pixel)
                 if (isLightColor(pixel)) {
                     setStatusBarLightMode(me, true)
                 }
             }
         }
     }

     override fun showTitle(title: String) {
         val forwardList = mWebView.copyBackForwardList()
         val item = forwardList.currentItem
         if (item != null) {
             val domainList = arrayOf(
                   "uclink://www.uc.cn/cc77796ca7c25dff9607d31b29effc07?action=open_url&src_pkg=lcyklc&src_scene=gp&src_ch=lcyklc4&src_scene=pullup&url=ext%3Ainfo_flow_open_channel%3Ach_id%3D100%26insert_item_ids%3D6255943528293392801%26type%3Dmultiple%26from%3D6001",
                   "给未来写封信",
                   "密码键盘",
                   "抬手唤醒"
               )
               if (item.title != null) {
                   if (Arrays.binarySearch(dnico.styTool.Utils.setToastToastUtils.showLong("0");
                   } else {
                      nico.styTool.Utils.setToast("1")
                   }
               }
               //nico.styTool.Utils.setToast(item.getTitle());
           }
         }
     }
 }

 private class YcX5WebViewClient(webView: X5WebView?, context: Context?) :
     JsX5WebViewClient(webView, context) {
     override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
         super.onReceivedSslError(view, handler, error)
         val url = error.url
         handler.proceed()
     }

     override fun onScaleChanged(view: WebView, oldScale: Float, newScale: Float) {
         super.onScaleChanged(view, oldScale, newScale)
         if (newScale - oldScale > 7) {
             view.setInitialScale((oldScale / newScale * 100).toInt())
         }
     }
 }

 override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
     super.onActivityResult(requestCode, resultCode, intent)
     if (requestCode == X5WebChromeClient.FILE_CHOOSER_RESULT_CODE) {
         mWebView.x5WebChromeClient.uploadMessage(intent, resultCode)
     } else if (requestCode == X5WebChromeClient.FILE_CHOOSER_RESULT_CODE_5) {
         mWebView.x5WebChromeClient.uploadMessageForAndroid5(intent, resultCode)
     }
 }

 public override fun onResume() {
     super.onResume()
     mWebView.onResume()
 }

 override fun onStop() {
     super.onStop()
     mWebView.settings.javaScriptEnabled = false
 }

 override fun onDestroy() {
     try {
         if (x5WebChromeClient != null) {
             x5WebChromeClient!!.removeVideoView()
         }
         val parent = mWebView.parent as ViewGroup
         parent.removeView(mWebView)
         mWebView.removeAllViews()
         mWebView.destroy()
     } catch (e: Exception) {
         nico.styTool.Utils.setToast(e.message)
     }
     super.onDestroy()
 }

 private fun handleLongImage(): Boolean {
     val hitTestResult = mWebView.hitTestResult
     if (hitTestResult.type == WebView.HitTestResult.IMAGE_TYPE || hitTestResult.type == WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
         val picUrl = hitTestResult.extra
         BottomMenu.show(this, arrayOf("查看大图|保存图片", "复制图片链接")) { _, index ->
             when (index) {
                 0 -> ImagePreview.getInstance()
                     .setContext(this@ActiveWeb)
                     .setImage(picUrl)
                     .start()
                 1 -> {
                     nico.styTool.Utils.setToast("复制成功")
                     ClipboardUtils.copyText(picUrl)
                 }
             }
         }
         return true
     }
     return false
 }

 private fun isLightColor(@ColorInt color: Int): Boolean {
     return 0.299 * Color.red(color) + 0.587 * Color.green(color) + 0.114 * Color.blue(color) >= 127.5
 }
}*/
package com.example.blade.activity.like

import com.example.blade.base.BaseFLazyActivity
import com.kongzue.baseframework.interfaces.Layout
import com.kongzue.baseframework.util.JumpParameter
import nico.styTool.R
import nico.styTool.fragment.SettingsFragment


@Layout(R.layout.settings_activity)
class LikeActivity : BaseFLazyActivity() {
    override fun setEvents() {

    }

    override fun initDatas(parameter: JumpParameter?) {

    }

    override fun initViews() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment( ))
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
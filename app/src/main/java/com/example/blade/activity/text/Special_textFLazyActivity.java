package com.example.blade.activity.text;

import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;

import com.example.blade.base.BaseFLazyActivity;
import com.google.android.material.navigation.NavigationView;
import com.kongzue.baseframework.interfaces.Layout;
import com.kongzue.baseframework.util.JumpParameter;

import nico.styTool.R;
import nico.styTool.Utils;

import static com.blankj.utilcode.util.ResourceUtils.readAssets2String;

@Layout(R.layout.activity_special_text)
public class Special_textFLazyActivity extends BaseFLazyActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_meizi:
                vft("ζั͡爱 ั͡✾", '爱');
                break;
            case R.id.nav_gank:
                vft("爱҉", '爱');
                break;
            case R.id.nav_about:
                vft("敏ۣۖ", '敏');
                break;
            case R.id.nav_se:
                vft("爱\n", '爱');
                break;
            case R.id.nav_blog:
                vft("‮ ‮ ‮爱", '爱');
                break;
            case R.id.nav_blogvip:
                vft("◤Q◢", 'Q');
                break;
            case R.id.nav_blogsvip:
                vft("❦H❧", 'H');
                break;
            case R.id.apktool_textview:
                vft(readAssets2String("Special_text/android_text_a.txt"), 'n');
                break;
            case R.id.apktool_textviewa:
                vft(readAssets2String("Special_text/android_text_b.txt"), 'n');
                break;
            case R.id.apktool_textviewb:
                vft(readAssets2String("Special_text/android_text_c.txt"), 'n');
                break;
            case R.id.a_upi:
                vft("妮̶", '妮');
                break;
        }
        return true;
    }

    private void vft(String aw, char bw) {
        EditText et = findViewById(R.id.mainEditText1);
        String os = et.getText().toString();
        char[] a = os.toCharArray();
        StringBuffer b = new StringBuffer();
        for (char anA : a) {
            b.append(aw.replace(bw, anA));
            Utils.INSTANCE.setClipboard(String.valueOf(b));
        }
        setTitle(b);
        // TextView t = (TextView) findViewById(R.id.mainTextView1);
        // t.setText(b);
    }

    @Override
    public void initViews() {
        NavigationView navigationView = findViewById(R.id.nv_main_navigation);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void initDatas(JumpParameter parameter) {

    }

    @Override
    public void setEvents() {

    }
}

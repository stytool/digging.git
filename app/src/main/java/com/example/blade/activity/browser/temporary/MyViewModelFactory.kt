package com.example.blade.activity.browser.temporary

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.just.agentweb.AgentWeb

class MyViewModelFactory(
    private val webViewActivity: WebViewActivity,
    private val mAgentWeb: AgentWeb
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(Int::class.java).newInstance(webViewActivity, mAgentWeb)
    }
}
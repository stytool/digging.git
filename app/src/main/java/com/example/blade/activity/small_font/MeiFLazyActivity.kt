package com.example.blade.activity.small_font

import android.text.SpannableString
import android.view.View
import android.widget.TextView
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.drake.engine.base.EngineActivity
import com.drake.engine.base.EngineSwipeActivity
import com.drake.engine.base.EngineToolbarActivity
import kotlinx.android.synthetic.main.me_main.*
import nico.styTool.R
import nico.styTool.Utils.setClipboard

class MeiFLazyActivity : EngineSwipeActivity<ViewDataBinding>(R.layout.me_main) {

    private val editText by lazy { mainEditText10 }

    override fun initData() {

    }

    override fun initView() {
        val layoutResId = android.R.layout.simple_list_item_activated_1
        val data = VIEW_ITEM.toMutableList()
        val recyclerView = api_id_views_listview.apply {
            layoutManager = LinearLayoutManager(this@MeiFLazyActivity)
        }
        val adapter = object : BaseQuickAdapter<String, BaseViewHolder>(layoutResId, data) {
            override fun convert(holder: BaseViewHolder, item: String) {
                holder.getView<TextView>(android.R.id.text1).apply {
                    text = item
                }
            }
        }
        recyclerView.adapter = adapter
        adapter.setOnItemClickListener { _, _, position ->
            startIntent(data[position])
        }
    }

    private fun startIntent(s: String) {
        val spannableString = SpannableString(s)
        val curds = editText.selectionStart
        editText.text.insert(curds, spannableString)
    }

    fun onC(view: View) {
        setClipboard(editText.text.toString())
    }

    companion object {
        private val VIEW_ITEM = arrayOf(
            "#这是在上面的小字#",
            "¹",
            "²",
            "³",
            "⁴",
            "⁵",
            "⁶",
            "⁷",
            "⁸",
            "⁹",
            "º",
            "#这是在下面的小字#",
            "₁",
            "₂",
            "₃",
            "₄",
            "₅",
            "₆",
            "₇",
            "₈",
            "₉",
            "₀",
            "#这是蓝字#",
            "🇦",
            "🇧",
            "🇨",
            "🇩",
            "🇪",
            "🇫",
            "🇬",
            "🇭",
            "🇮",
            "🇯",
            "🇰",
            "🇱",
            "🇲",
            "🇳",
            "🇴",
            "🇵",
            "🇶",
            "🇷",
            "🇸",
            "🇹",
            "🇺",
            "🇻",
            "🇼",
            "🇽",
            "🇾",
            "🇿"
        )
    }
}
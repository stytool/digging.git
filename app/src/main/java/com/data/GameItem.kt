package com.data

data class GameItem(
    val cname: String,
    val ename: Int,
    val hero_type: Int,
    val hero_type2: Int,
    val new_type: Int,
    val pay_type: Int,
    val skin_name: String,
    val title: String
)